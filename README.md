# Arms Race

Collection of scripts for managing matrix rooms

**Whatever you do don't commit your config**

You probably want to run these scripts in a Python 3 venv:
```shell
$ python3 -m venv env
$ source env/bin/activate
(env) $ pip install matrix_client
```

Sample config
```json
{
	"username":"user",
	"password":"pass",
	"content": "thing to look for",
	"rooms": [
		"#dia-editor:matrix.org"
	]
}
```

Based on the original scripts thrown back and forward through matrix as we
fought of a flood of IRC spam

None of this would be possible without the excellent [Matrix Python SDK](https://matrix-org.github.io/matrix-python-sdk/) to work with matrix, IRC users fend for yourselves!

Thanks to [Felix Häcker](https://gitlab.gnome.org/haecker-felix) for helping during the flood and [Alexander Mikhaylenko](https://gitlab.gnome.org/exalm) for their revisions

Original scripts:
* ["Fight them with scripts"](https://gitlab.gnome.org/snippets/641) First 'public' version
* ["Updated antispam script"](https://gitlab.gnome.org/snippets/642) Improved by @exalm
* ["Updated antispam script (backlog version)"](https://gitlab.gnome.org/snippets/643) Retrospectively redact messages