import csv
import time


class ArmsLog:
    def __init__(self, name):
        self.name = name
        self.count = 0


    def log (self, room, action, user, message=''):
        ts = time.strftime('%b %d %Y %H:%M:%S')

        print("{:4}  {}: {:25} - {:10} {} {}".format(self.count,
                                                     ts,
                                                     room,
                                                     action,
                                                     user,
                                                     message))
        self.count += 1

        with open(self.name, 'a') as csvfile:
            w = csv.writer(csvfile)
            w.writerow([ts, room, action, user, message])
