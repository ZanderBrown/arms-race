#!/bin/env python3

from matrix_client.client import MatrixClient
from matrix_client.api import MatrixRequestError
import json
import time
import armlog

client = MatrixClient("http://matrix.org")

log = armlog.ArmsLog('maneuvers.log')

data = {}
with open('login.json') as f:
    data = json.load(f)

token = client.login(username=data["username"], password=data["password"])

def on_message(room, event):
    global log
    if event['type'] == "m.room.message":
        if event['content'] != {} and event['content']['msgtype'] == "m.text":
            if data['content'] in event['content']['body']:
                try:
                    room.redact_message(event['event_id'])
                    if room.kick_user(event['sender']):
                        log.log(room._hack_name, 'kick', event['sender'])
                    else:
                        log.log(room._hack_name,
                                'spam',
                                event['sender'],
                                'Failed to kick')
                except MatrixRequestError as e:
                    log.log(room._hack_name,
                            'spam',
                            event['sender'],
                            'Failed to redact message')


for roomname in data['rooms']:
    room = client.join_room(roomname)
    # Matrix seems to have an issue with names, use a hack to track it
    room._hack_name = roomname
    room.add_listener(on_message)
    log.log(room._hack_name,
            'connected',
            '[arms-race]')

log.log('', 'start', '[arms-race]')

client.start_listener_thread()

while True:
    time.sleep(1)
